@echo off
pushd "%~dp0"

:: Default values .. /silent /nonlegacy /com9
set InstallMode=qn
set VFILegacy=0
set VFIPort=9
set VFIMultiDevice=0
set VFIReadIntTO=0
set VFIUsbNoZeroWrite=0
set comstring=/com9
set VFIPortParam=9
set VFIMsi=VFI_USB_Driver_Installer_2.1.0.1_B2.msi
set VFIMinFootPrint=0
SET WINOS=0

set /a i=1 
rem echo parameters=%*
rem echo comstring=%comstring% 
rem pause
if "%1"=="" goto paramFinished

:getParam
set param=%1
rem echo param=%param%

if /i "%1"=="/silent" (
	rem echo silent mode
	set InstallMode=qn
) else if /i "%1"=="/legacy" (
	rem echo legacy mode
	set VFILegacy=1
) else if /i "%1"=="/VFILegacy" (
	rem echo legacy mode
	set VFILegacy=1
) else if /i "%1"=="/nonlegacy" (
	rem echo nonlegacy mode
	set VFILegacy=0
) else if /i "%1"=="/VfiReadIntTO" (
	rem echo VFIReadIntTO mode
	set VFIReadIntTO=1
) else if /i "%1"=="/NOZEROWRITE" (
	rem echo VFIUsbNoZeroWrite mode
	set VFIUsbNoZeroWrite=1
) else if /i "%1"=="/VFIUsbNoZeroWrite" (
	rem echo VFIUsbNoZeroWrite mode
	set VFIUsbNoZeroWrite=1
) else if /i "%1"=="/MULTIDEV" (
	rem echo MultiDevice mode
	set VFIMultiDevice=1
) else if /i "%1"=="/VFIMultiDevice" (
	rem echo MultiDevice mode
	set VFIMultiDevice=1
) else if /i "%1"=="/VFIMinFootPrint" (
	rem echo MultiDevice mode
	set VFIMinFootPrint=1
) else if /i "%param:~0,4%"=="%comstring:~0,4%" (
	rem echo subParam COM mode COM%param:~4,3% 
	if /i "%param:~4,3%"=="" (
		rem echo com param is invalid
	) else (
		rem echo com has a value
		set VFIPort=%param:~4,3% 
	)	
 ) else if /i "%param:~0,8%"=="/VFIPort" (
	rem echo subParam COM mode COM%param:~8,3% 
	if /i "%param:~8,3%"=="" (
		rem echo com param is invalid
	) else (
		rem echo com has a value
		set VFIPort=%param:~8,3% 
	)
 )


shift
if "%1"=="" goto paramFinished
set /a i=%i%+1

goto getParam
:paramFinished

:: Get windows Version numbers
For /f "tokens=2 delims=[]" %%G in ('ver') Do (set WinVersion=%%G) 
For /f "tokens=2,3,4 delims=. " %%G in ('echo %WinVersion%') Do (set WinMajor=%%G& set WinMinor=%%H& set WinBuild=%%I) 
Set osver=%WinMajor%.%WinMinor%

IF %osver%==10.0 ( 
	SET WINOS=1

)
::Uninstall 2.0.0.12_B2 if present.

msiexec.exe /qn /x {92073449-0D0E-4674-A886-83174846434A}  /L*v+ "%temp%\VFI_USB_Driver_UnInstaller.log"

::Uninstall 2.1.X.X if present.
msiexec.exe /qn /x {013CA33B-C8C5-42C3-93EA-D6AF18395F9E} WINOS=%WINOS% /L*v+ "%temp%\VFI_USB_Driver_UnInstaller.log"
msiexec /%InstallMode% /i %VFIMsi% WINOS=%WINOS% VFIPort=%VFIPort% VFIReadIntTO=%VFIReadIntTO%  VFILegacy=%VFILegacy% VFIUsbNoZeroWrite=%VFIUsbNoZeroWrite% VFIMultiDevice=%VFIMultiDevice% /L*v+ "%temp%\VFI_USB_Driver_Installer.log"

popd

:: Reset the variables
set VFIPortParam=	
set param=
set subParam=
set comstring=
set InstallMode=
set VFILegacy=
set VFIPort=
set VFIMultiDevice=
set VFIReadIntTO=
set VFIUsbNoZeroWrite=
set VFIMinFootPrint=
SET WINOS=

goto :EndComment



Parameters used in the Install batch files.
-------------------------------------------------------------------
Following parameters can be used to set different properties in VFI USB driver.

1. For setting the COM port, 
	VFIPortX : set default port value (COM 9), if not already set

        Open a command prompt with administrator privileges and 
        pass /VFIPortX to silent.bat file as below

	eg: silent.bat /VFIPortX
	Where X represents com port number ranging from 1 to 256.
 
2. Following are the parameters for setting different registry values.
  
   a. To set VFILegacy Registry values 
       Parameter can be used    :    VFILegacy
       
       To set VFILegacy Registry values , Open a command prompt with 
       administrator privileges and pass /VFILegacy to silent.bat file as below
       (This will set registry value to 1): Default value(0 )
       eg :  silent.bat /VFILegacy 

   b. To set VFIReadIntTO  Registry values
       Parameter can be used    :    VFIReadIntTO
       
       To set VFIReadIntTO  Registry values, Open a command prompt with 
       administrator privileges and pass /VFIReadIntTO to silent.bat file as below 
       (This will set registry value to 1) : Default value(0)
       eg :  silent.bat /VFIReadIntTO 

   c. To set VFIUsbNoZeroWrite Registry values
       Parameter can be used    :    VFIUsbNoZeroWrite 

      To set VFIUsbNoZeroWrite Registry values, Open a command prompt with 
      administrator privileges and pass /VFIUsbNoZeroWrite to silent.bat file as below 
      (This will set registry value to 1) : Default value(0)
      eg :  silent.bat /VFIUsbNoZeroWrite 

   d. To set VFIMultiDevice  Registry values
       Parameter can be used    :    VFIMultiDevice

      To set VFIMultiDevice  Registry values, Open a command prompt with 
      administrator privileges and pass /VFIMultiDevice to silent.bat file as below 
      (This will set registry value to 1) : Default value(0)
      eg :  silent.bat /VFIMultiDevice  

3. For enabling Minimize FootPrint in Windows XP Embedded OS,
	   Parameter can be used    :    VFIMinFootPrint
	   eg :  silent.bat /VFIMinFootPrint  
 
	   
	  
:EndComment