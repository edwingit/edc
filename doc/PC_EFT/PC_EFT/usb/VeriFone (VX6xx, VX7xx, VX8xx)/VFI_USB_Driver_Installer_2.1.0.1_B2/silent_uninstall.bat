@echo off

pushd "%~dp0"

REM silent uninstall using product code
SET WINOS=0

:: Get windows Version numbers
For /f "tokens=2 delims=[]" %%G in ('ver') Do (set WinVersion=%%G) 
For /f "tokens=2,3,4 delims=. " %%G in ('echo %WinVersion%') Do (set WinMajor=%%G& set WinMinor=%%H& set WinBuild=%%I) 
Set osver=%WinMajor%.%WinMinor%

IF %osver%==10.0 ( 
	SET WINOS=1

)

::Uninstall 2.0.0.12_B2 if present.

msiexec.exe /qn /x {92073449-0D0E-4674-A886-83174846434A}  /L*v+ "%temp%\VFI_USB_Driver_UnInstaller.log"
msiexec.exe /qn /x {013CA33B-C8C5-42C3-93EA-D6AF18395F9E} WINOS=%WINOS% /L*v+ "%temp%\VFI_USB_Driver_UnInstaller.log"


SET WINOS=

popd
