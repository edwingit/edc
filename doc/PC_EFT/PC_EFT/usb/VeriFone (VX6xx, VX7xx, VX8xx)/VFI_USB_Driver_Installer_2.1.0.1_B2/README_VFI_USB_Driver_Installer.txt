VFI USB Driver : version 2.1.0.1
Build 2, July 14th 2015
-------------------------------------------

VFI USB Driver is built on KMDF. The Kernel-Mode Driver Framework (KMDF) is a driver framework
developed by Microsoft as a tool to aid driver developers create and maintain kernel mode 
device drivers for Windows 2000 and later releases.
VFI USB Driver is used to communicate between the Vx , Mx series of terminals to the host 
windows machine.

Software requirements and notes :
---------------------------------

- Windows Installer version recommended for installing MSI package is .
  "Windows Installer V 4.5"
- The installation/uninstall MUST be performed with administrative privileges
- We recommend connecting your VeriFone device after finishing this
   installation, but if you have already connected it then simply cancel
   any "Found New Hardware" dialogs that pops up.

This release supports :
-----------------------

           Windows platforms :  

                    Windows 10 Pro Insider Preview         Microsoft signed
                    Win 8.1( 32 bit & 64 bit)              Microsoft signed
                    Win 8( 32 bit & 64 bit)                Microsoft signed
                    Win 7( 32 bit & 64 bit)                Microsoft signed
                    Win Vista( 32 bit & 64 bit)            Microsoft signed
                    Win server 2003 (( 32 bit & 64 bit))   Microsoft signed
                    Win XP( 32 bit & 64 bit)               Microsoft signed
                    POSReady 2009                          Microsoft signed
                    WEPOS                                  Microsoft signed
                    Windows Embedded POSReady 7(32bit)     Microsoft signed


		
 	
	Terminal Series	:
	-----------------
       
        Vx (both predator and eVo ).
        ---------------------------
	VX 510 Ethernet Terminal 
	VX 510 GPRS Terminal 
	VX 610 Terminal 
	VX 570 Terminal 
	VX 670 Terminal 
	VX 700 Terminal 
	VX 800 PIN Pad 
	VX 810 PIN Pad 
	VX 680 Terminal 
	VX 520 Terminal 
	VX 820 PIN pad 
	VX 805 Terminal 
	VX 825 Terminal 
	VX 520 GPRS Terminal 
	VX 680 CDMA Terminal  
	FD55 Terminal 
	VX 680 BlueTooth Terminal 
	VX 680 WiFi Terminal 
	VX 520 Sprocket Printer Terminal 
	VX 675 Terminal 
	VX 680 3G Terminal 
	VX GEN3 Terminal 
	VX 675 3G Terminal 
	VX 820 DUET base Terminal
	VX 805 DUET base Terminal
	VX 690 3G Terminal
	VX 685 Terminal
	VSP100 Terminal
	VX 600 payware mobile reader
	VX 690 BASE
	PP1000 Next Gen

	Mx and UX
	----------
	Mx8 Terminal
	Mx9 Terminal
	Ux100 Terminal
	Ux200 Terminal
	Ux300 Terminal
		
Usage:
------

A. Un-Installation of any previous VeriFone Vx and Mx drivers.
--------------------------------------------------------------

  Exit all the application that is using COM port, including Spytool, Port monitors.
  Refer Un-Installation section in the respective Release notes of the VeriFone Vx driver 
  and Release notes of the VeriFone Mx driver. 

Note:

After un-installation, make sure the device is listed under as "Other devices" in Device Manager. 

B. Installation of the VFI USB Driver
-------------------------------------

Installation through silent.bat is highly recommended.


Steps for installation

Method 1:

   1. Download the zip file that is provided with the release. 
   2. Extract the contents of the zip file to any folder.
   3. Open a command prompt (Recommended with administrator privileges).
   4. Switch to the folder which specified in step 2.
   5. Run the batch file "silent.bat".   
   6. The driver will be installed without any UI.   
   7. Wait for the installation to complete.
   8. Connect the Vx terminal/Vx PINpad/Mx terminal using USB and power up the terminal. 

Method 2: (Only for Systems using RedSys web application(Spain))

   1. Download the zip file that is provided with the release. 
   2. Extract the contents of the zip file to any folder.
   3. Open a command prompt (Recommended with administrator privileges).
   4. Switch to the folder which specified in step 2.
   5. Run the batch file "silent_vfiTO.bat".   
   6. The driver will be installed without any UI.   
   7. Wait for the installation to complete.
   8. Connect the Vx terminal/Vx PINpad/Mx terminal using USB and power up the terminal. 

  Note: 

   1. Systems using RedSys web application(Spain), a separate installation 
      batch file(silent_vfiTO.bat) have been given to set the variable 
      required to fix communication issue reported.
   2. Use this batch file silent_vfiTO.bat only in the respective use case.
   3. Using silent_vfiTO.bat will install to COM2,Please ensure that 
      any physical port(COM2)present should be disabled prior to installation of driver.

C. Un-Installation of the driver VFI USB Driver
-----------------------------------------------

Installed drivers can be uninstalled from:

Method 1:- Silent uninstall:

   1. Download the zip file that is provided with the release. 
   2. Extract the contents of the zip file to any folder.
   3. Open a command prompt (Recommended only with administrator privileges).
   4. Switch to the folder which specified in step 2.
   5. Run the batch file "silent_uninstall.bat".   
   6. The driver will be un-installed without any UI.   
   7. Wait for the un-installation to complete.

Note:

After un-installation, make sure the device is listed under as "Other devices" in Device Manager. 

Parameters used in the Install batch files.
-------------------------------------------

Following parameters can be used to set different properties in VFI USB driver.

1. For setting the COM port, 
	VFIPortX : set default port value (COM 9), if not already set

        Open a command prompt with administrator privileges and 
        pass /VFIPortX to silent.bat file as below

	eg: silent.bat /VFIPortX
	Where X represents com port number ranging from 1 to 256.
 
2. Following are the parameters for setting different registry values.
  
   a. To set VFILegacy Registry values 
       Parameter can be used    :    VFILegacy
       
       To set VFILegacy Registry values , Open a command prompt with 
       administrator privileges and pass /VFILegacy to silent.bat file as below
       (This will set registry value to 1): Default value(0 )
       eg :  silent.bat /VFILegacy 

   b. To set VFIReadIntTO  Registry values
       Parameter can be used    :    VFIReadIntTO
       
       To set VFIReadIntTO  Registry values, Open a command prompt with 
       administrator privileges and pass /VFIReadIntTO to silent.bat file as below 
       (This will set registry value to 1) : Default value(0)
       eg :  silent.bat /VFIReadIntTO 

   c. To set VFIUsbNoZeroWrite Registry values
       Parameter can be used    :    VFIUsbNoZeroWrite 

      To set VFIUsbNoZeroWrite Registry values, Open a command prompt with 
      administrator privileges and pass /VFIUsbNoZeroWrite to silent.bat file as below 
      (This will set registry value to 1) : Default value(0)
      eg :  silent.bat /VFIUsbNoZeroWrite 

   d. To set VFIMultiDevice  Registry values
       Parameter can be used    :    VFIMultiDevice

      To set VFIMultiDevice  Registry values, Open a command prompt with 
      administrator privileges and pass /VFIMultiDevice to silent.bat file as below 
      (This will set registry value to 1) : Default value(0)
      eg :  silent.bat /VFIMultiDevice  

3. For enabling Minimize FootPrint in Windows XP Embedded OS,
	   Parameter can be used    :    VFIMinFootPrint
	   eg :  silent.bat /VFIMinFootPrint  


Change history
--------------

2.1.0
-----

1. Added support for Windows 10 OS.


2.0.0
-----

1. Supporting Vx, Mx and Ux series of terminals.

2. Multidevice support : Enabled to communicate with multiple Verifone 
   terminals (Mx and Vx) simultaneously

3. Installation process is silent without any DOS message box from PC drive
   if used silent.bat file

4. Windows Vista support added.

5. Support added for PP1000NG device having OS QT10020T.

6. Support added for all supported Vx devices having OS QT820245(Refer USB_Driver_Reference_Guide.chm for supported Vx devices).

7. Implemented WPP Software Tracing. Events that pass non critical information will be
   available from the Driver WPP logs.
   Refer the document , VFI USB_Driver_Reference_Guide.chm under Troubleshooting 
   for more details .
   
8. Enhancement done to support legacy applications (built using VB & mscomm32.ocx library) . 


Known Limitations:
------------------

1. If installed without Administrative privilege, installation will not happen correctly.

2. If silent.bat/silent_uninstall.bat file runs without Administrative privilege, 
   command windows will pop up and installation will not happen correctly.
