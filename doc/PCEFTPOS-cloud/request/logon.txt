end point :
https://rest.pos.sandbox.cloud.pceftpos.com/v1/sessions/3B513173-C969-404C-8B50-55547A76CE7D/logon?async=false

Header:
Content-Type : application/json
Accept			:	application/json
Authorization	:	Bearer eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2Q0JDLUhTNTEyIiwidHlwIjoiSldUIn0..FiHc1MMs_vgguKwoVrZorw.qrZtoIUfCe_MTL-1drID-nPjmieZA7mc4f1R3cUl19FlR5wR-iCRFF5xPHRDOGrrmcs673JfzLRTYJUx5scsN8WlDceBkuC59bxogF6T5KiFMvDbF3e4w3QMv9s0u-L3zjc0PA9ln2tFdVytjWTWnZGhIl8AHD1O9N3f5_zFJ3uwnpZ99CTC5FAkLtIqtaHc3Ny9Gp_dtQfab91tzbiQngvMKqYvPz54BjtYiePxSgWw0JGpGzdrhsMI1OG3xe_Tn1KawzoRS0lObRSkBJSUyGXK40SU1XmGH5lMVsyJmDwH_10I4YNfCNmMYD-utR4k9RvtOLIRe8d_7ysrGSgDhWQZDGI5EiuSIgquDRJgX-NB-qChvbcn4j6-PpsDZiEqwT6lKRd1ArGWQzm9BhGhOjCH16jDk4zoxutRDIQJ7OpnypYVX5zxPBHGtp0jJRrz001LUM9qAf-6J3GPPELJ0dPgTZVHgHulJ4QfmOjnudBOhG3RxHW42OxS5j4Es8WPajqGHvYAQwR0_94El_ZIfiTWDAyC-g5euqsMAEvUVEz8z5eNPGpUYDz2M-EjxJiigOhsZyzJMXcl0awzZMFPTF3_uZt62EbaUNK9mAlGRpKMkFn-O5U1Da3iy8EbyYTvttGFO9ej4M2oXCFrKTEodHLNcOikjIvMkI3puIaZr-t3Vtaj3PQk18uJPo8aC5cFKMhu_E7llM1ijKuipZnDOJD-ZNn9fZKsvB0Gw7eGBQ0Ckmh27VdphAsD1jKOhCwutRnffNKCUvUGMTPUHl_gCGDdoa4JBDqyw3l_MyEsz_0dP52kJsVbaB3v1YBMfGMrioBNWdeFs0TABoqRFsU-ag.eG9Z755oLPMD-r50Nx7a9z1RZlIY5niXxLVNdJLbvqo


Request Body
====================
{
    "Request": 
    {
        "AmtPurchase": 100,
        "TxnType": "P",
        "TxnRef": "1234567890"
    },
    "Notification":
    {
        "Uri": "https://myPos.com.au/{{sessionid}}/{{type}}",
        "AuthorizationHeader": "Bearer <<token>>"
    }
}

Response :
================================================ 
{
    "responseType": "logon",
    "response": {
        "pinPadVersion": "100800          ",
        "success": true,
        "responseCode": "00",
        "responseText": "APPROVED            ",
        "date": "0001-01-01T00:00:00",
        "catid": "12345678",
        "caid": "123456789012345",
        "stan": 0,
        "purchaseAnalysisData": {}
    }
}