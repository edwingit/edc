<?php

    namespace App\Library\Bases; 

    use App\Models\User;
    use App\Models\OrderPayment;
    use App\Models\Payment;

    use App\Library\Constants\ApiMessage;
    use App\Library\Constants\ApiCode;

    abstract class BasePayment
    {
        public $error_type;
        public $error_messages;

        public $output;
        public $result;

        public $message_type;
        public $http_code;
        public $http_method;
        public $status;
        public $response_code;

        public $api_payload;
        public $api_response;

        public $is_logtofile;

        public $payment_id;

        protected $outlet_id;

        public function setDataNotFoundError()
        {
            $this->response_code = ApiCode::OUT_ERR;
            $this->http_code = 404;
            $this->message_type = ApiMessage::OUT_ERR;
            return false;
        }
    }