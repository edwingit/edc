-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 11 Nov 2019 pada 10.02
-- Versi server: 5.6.17
-- Versi PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pceftpos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `apps`
--

DROP TABLE IF EXISTS `apps`;
CREATE TABLE IF NOT EXISTS `apps` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `apps`
--

INSERT INTO `apps` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(1, 'smarttab-ios', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `apps` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(2, 'smarttab-android', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `apps` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(3, 'kiosk-ios', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `apps` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(4, 'kiosk-android', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_permissions`
--

DROP TABLE IF EXISTS `app_permissions`;
CREATE TABLE IF NOT EXISTS `app_permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `app_id` int(11) NOT NULL,
  `app_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app_permissions_app_id_index` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `app_permissions`
--

INSERT INTO `app_permissions` (`id`, `app_id`, `app_key`, `app_secret`, `is_active`, `created_at`, `updated_at`) VALUES(1, 3, '9c6a6677-2d4a-473a-bb53-1df09ad032b1', 'e1ded469-654d-42e9-f546-de521c60bc30', 1, '2019-09-04 00:00:00', '2019-09-04 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_logs`
--

DROP TABLE IF EXISTS `auth_logs`;
CREATE TABLE IF NOT EXISTS `auth_logs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `origin_merchant_id` int(11) NOT NULL,
  `app_id` tinyint(4) NOT NULL,
  `auth_header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `response_code` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_logs_app_id_index` (`app_id`),
  KEY `auth_logs_response_code_index` (`response_code`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `auth_logs`
--

INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(1, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'ERR_PERM', 9900, '2019-11-10 13:03:14', '2019-11-10 13:03:14');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(2, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:03:32', '2019-11-10 13:03:32');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(3, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:21:56', '2019-11-10 13:21:56');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(4, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:22:08', '2019-11-10 13:22:08');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(5, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:22:24', '2019-11-10 13:22:24');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(6, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:23:02', '2019-11-10 13:23:02');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(7, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:23:10', '2019-11-10 13:23:10');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(8, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:23:17', '2019-11-10 13:23:17');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(9, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:29:47', '2019-11-10 13:29:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(10, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:30:23', '2019-11-10 13:30:23');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(11, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:31:01', '2019-11-10 13:31:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(12, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:31:16', '2019-11-10 13:31:16');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(13, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:31:53', '2019-11-10 13:31:53');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(14, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:32:20', '2019-11-10 13:32:20');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(15, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:34:08', '2019-11-10 13:34:08');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(16, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:35:15', '2019-11-10 13:35:15');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(17, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:35:43', '2019-11-10 13:35:43');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(18, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:35:54', '2019-11-10 13:35:54');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(19, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:36:17', '2019-11-10 13:36:17');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(20, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:36:26', '2019-11-10 13:36:26');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(21, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:37:03', '2019-11-10 13:37:03');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(22, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:37:19', '2019-11-10 13:37:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(23, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:38:44', '2019-11-10 13:38:44');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(24, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:39:33', '2019-11-10 13:39:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(25, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:41:04', '2019-11-10 13:41:04');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(26, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:45:06', '2019-11-10 13:45:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(27, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:46:01', '2019-11-10 13:46:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(28, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:47:36', '2019-11-10 13:47:36');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(29, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:47:47', '2019-11-10 13:47:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(30, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:51:30', '2019-11-10 13:51:30');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(31, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:52:01', '2019-11-10 13:52:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(32, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:52:49', '2019-11-10 13:52:49');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(33, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:53:14', '2019-11-10 13:53:14');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(34, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:53:26', '2019-11-10 13:53:26');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(35, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:53:50', '2019-11-10 13:53:50');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(36, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 13:54:29', '2019-11-10 13:54:29');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(37, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 03:00:47', '2019-11-11 03:00:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(38, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 03:02:07', '2019-11-11 03:02:07');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(39, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 03:02:19', '2019-11-11 03:02:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(40, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 03:03:00', '2019-11-11 03:03:00');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(41, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 03:06:32', '2019-11-11 03:06:32');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(42, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 03:07:11', '2019-11-11 03:07:11');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(43, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 03:08:45', '2019-11-11 03:08:45');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(44, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:37:25', '2019-11-11 04:37:25');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(45, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:41:15', '2019-11-11 04:41:15');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(46, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:41:27', '2019-11-11 04:41:27');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(47, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:42:09', '2019-11-11 04:42:09');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(48, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:42:35', '2019-11-11 04:42:35');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(49, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:42:57', '2019-11-11 04:42:57');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(50, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:43:41', '2019-11-11 04:43:41');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(51, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:44:14', '2019-11-11 04:44:14');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(52, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:49:50', '2019-11-11 04:49:50');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(53, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:53:23', '2019-11-11 04:53:23');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(54, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:53:41', '2019-11-11 04:53:41');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(55, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:54:17', '2019-11-11 04:54:17');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(56, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:54:37', '2019-11-11 04:54:37');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(57, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:54:57', '2019-11-11 04:54:57');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(58, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:55:24', '2019-11-11 04:55:24');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(59, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:56:10', '2019-11-11 04:56:10');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(60, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:56:42', '2019-11-11 04:56:42');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(61, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:56:59', '2019-11-11 04:56:59');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(62, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:58:04', '2019-11-11 04:58:04');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(63, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:58:28', '2019-11-11 04:58:28');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(64, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:58:59', '2019-11-11 04:58:59');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(65, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 04:59:46', '2019-11-11 04:59:46');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(66, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:00:00', '2019-11-11 05:00:00');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(67, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:00:08', '2019-11-11 05:00:08');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(68, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:00:42', '2019-11-11 05:00:42');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(69, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:00:52', '2019-11-11 05:00:52');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(70, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:01:14', '2019-11-11 05:01:14');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(71, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:02:20', '2019-11-11 05:02:20');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(72, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:03:00', '2019-11-11 05:03:00');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(73, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:03:47', '2019-11-11 05:03:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(74, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:04:51', '2019-11-11 05:04:51');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(75, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:06:10', '2019-11-11 05:06:10');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(76, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:06:34', '2019-11-11 05:06:34');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(77, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:07:43', '2019-11-11 05:07:43');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(78, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:08:17', '2019-11-11 05:08:17');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(79, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:10:40', '2019-11-11 05:10:40');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(80, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:10:48', '2019-11-11 05:10:48');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(81, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:11:21', '2019-11-11 05:11:21');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(82, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:12:16', '2019-11-11 05:12:16');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(83, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:12:37', '2019-11-11 05:12:37');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(84, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:12:52', '2019-11-11 05:12:52');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(85, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:15:52', '2019-11-11 05:15:52');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(86, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:16:48', '2019-11-11 05:16:48');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(87, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:19:43', '2019-11-11 05:19:43');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(88, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:21:03', '2019-11-11 05:21:03');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(89, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:21:34', '2019-11-11 05:21:34');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(90, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 05:22:51', '2019-11-11 05:22:51');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(91, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 06:42:44', '2019-11-11 06:42:44');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(92, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 06:43:17', '2019-11-11 06:43:17');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(93, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 06:43:58', '2019-11-11 06:43:58');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(94, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 06:45:03', '2019-11-11 06:45:03');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(95, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 06:46:36', '2019-11-11 06:46:36');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(96, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 06:49:24', '2019-11-11 06:49:24');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(97, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 06:53:29', '2019-11-11 06:53:29');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(98, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 06:55:40', '2019-11-11 06:55:40');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(99, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 06:56:32', '2019-11-11 06:56:32');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(100, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:01:28', '2019-11-11 07:01:28');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(101, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:02:13', '2019-11-11 07:02:13');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(102, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:02:57', '2019-11-11 07:02:57');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(103, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:04:40', '2019-11-11 07:04:40');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(104, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:05:53', '2019-11-11 07:05:53');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(105, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:06:36', '2019-11-11 07:06:36');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(106, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:07:50', '2019-11-11 07:07:50');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(107, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:08:48', '2019-11-11 07:08:48');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(108, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:09:38', '2019-11-11 07:09:38');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(109, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:12:58', '2019-11-11 07:12:58');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(110, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:14:14', '2019-11-11 07:14:14');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(111, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:18:33', '2019-11-11 07:18:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(112, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:19:36', '2019-11-11 07:19:36');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(113, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:21:08', '2019-11-11 07:21:08');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(114, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:21:46', '2019-11-11 07:21:46');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(115, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:23:41', '2019-11-11 07:23:41');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(116, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:26:11', '2019-11-11 07:26:11');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(117, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:28:22', '2019-11-11 07:28:22');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(118, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:30:24', '2019-11-11 07:30:24');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(119, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:31:11', '2019-11-11 07:31:11');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(120, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:32:01', '2019-11-11 07:32:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(121, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:33:35', '2019-11-11 07:33:35');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(122, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:35:29', '2019-11-11 07:35:29');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(123, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 07:36:22', '2019-11-11 07:36:22');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(124, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 09:16:32', '2019-11-11 09:16:32');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(125, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 09:17:10', '2019-11-11 09:17:10');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(126, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 09:17:39', '2019-11-11 09:17:39');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(127, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 09:46:54', '2019-11-11 09:46:54');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(128, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 09:47:08', '2019-11-11 09:47:08');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(129, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 09:47:48', '2019-11-11 09:47:48');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(130, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 09:48:29', '2019-11-11 09:48:29');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(131, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 09:59:20', '2019-11-11 09:59:20');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(132, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 10:00:06', '2019-11-11 10:00:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(133, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 10:01:04', '2019-11-11 10:01:04');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(134, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-11 10:01:42', '2019-11-11 10:01:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `configs`
--

DROP TABLE IF EXISTS `configs`;
CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `partner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `partner_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `station_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pos_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_version` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `terminal_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) DEFAULT '0',
  `merchant_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `configs_merchant_id_index` (`merchant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `configs`
--

INSERT INTO `configs` (`id`, `partner_id`, `partner_secret`, `station_id`, `pos_id`, `pos_name`, `pos_version`, `terminal_id`, `api_url`, `currency`, `timezone`, `is_active`, `merchant_id`, `created_at`, `updated_at`) VALUES(1, '10100280001', 'FBXIUAVEIH8G7DSC', '608050', '3e7f5001-58a3-43fa-9129-6e84a7b4f2a0', 'POS Tabsquare', '12.6.80.18', 'K1', 'https://rest.pos.sandbox.cloud.pceftpos.com/v1/sessions/', 'NZD', '', 1, 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `configs` (`id`, `partner_id`, `partner_secret`, `station_id`, `pos_id`, `pos_name`, `pos_version`, `terminal_id`, `api_url`, `currency`, `timezone`, `is_active`, `merchant_id`, `created_at`, `updated_at`) VALUES(2, '10100280001', 'FBXIUAVEIH8G7DSC', '608051', '3e7f5001-58a3-43fa-9129-6e84a7b4f2a0', 'POS Tabsquare', '12.6.80.18', 'K2', 'https://rest.pos.sandbox.cloud.pceftpos.com/v1/sessions/', 'NZD', '', 1, 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_id_gopay` bigint(20) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `total_amount` decimal(10,2) NOT NULL,
  `table_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `msg_id` text COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gopay_txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qr_code` text COLLATE utf8_unicode_ci,
  `url_gopay_status` text COLLATE utf8_unicode_ci NOT NULL,
  `url_gopay_cancel` text COLLATE utf8_unicode_ci NOT NULL,
  `url_gopay_update` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1 - Success Generate, 2 - Success Reversal  3 - Sucess Refund, 4 - Success Query',
  `request` text COLLATE utf8_unicode_ci NOT NULL,
  `response` text COLLATE utf8_unicode_ci NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `client_info` text COLLATE utf8_unicode_ci NOT NULL,
  `op_type` tinyint(4) NOT NULL,
  `app_id` tinyint(4) NOT NULL,
  `txn_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_order_id_index` (`order_id`),
  KEY `logs_order_id_gopay_index` (`order_id_gopay`),
  KEY `logs_merchant_id_index` (`merchant_id`),
  KEY `logs_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(7, '2019_09_03_072530_create_apps_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(8, '2019_09_03_072621_create_app_permissions_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(9, '2019_09_03_073458_create_configs_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(10, '2019_09_03_073544_create_logs_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(11, '2019_09_03_073736_create_transactions_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(12, '2019_11_08_171826_create_auth_logs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_id_gopay` bigint(20) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `total_amount` decimal(10,2) NOT NULL,
  `table_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `msg_id` text COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gopay_txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qr_code` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL COMMENT '0 - Unpaid, 1 - Paid, 2 - Reversed',
  `merchant_id` int(11) NOT NULL,
  `client_info` text COLLATE utf8_unicode_ci NOT NULL,
  `app_id` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_order_id_index` (`order_id`),
  KEY `transactions_order_id_gopay_index` (`order_id_gopay`),
  KEY `transactions_merchant_id_index` (`merchant_id`),
  KEY `transactions_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
