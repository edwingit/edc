-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 24 Jun 2020 pada 16.31
-- Versi server: 5.7.26
-- Versi PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adyen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_permissions`
--

DROP TABLE IF EXISTS `app_permissions`;
CREATE TABLE IF NOT EXISTS `app_permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `app_id` int(11) NOT NULL,
  `app_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app_permissions_app_id_index` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `app_permissions`
--

INSERT INTO `app_permissions` (`id`, `app_id`, `app_key`, `app_secret`, `is_active`, `created_at`, `updated_at`) VALUES(1, 3, '9c6a6677-2d4a-473a-bb53-1df09ad032b1', 'e1ded469-654d-42e9-f546-de521c60bc30', 1, '2019-09-04 00:00:00', '2019-09-04 00:00:00');
INSERT INTO `app_permissions` (`id`, `app_id`, `app_key`, `app_secret`, `is_active`, `created_at`, `updated_at`) VALUES(2, 4, '9c6a6677-2d4a-473a-bb53-1df09ad032b1', 'e1ded469-654d-42e9-f546-de521c60bc30', 1, '2019-09-04 00:00:00', '2019-09-04 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_products`
--

DROP TABLE IF EXISTS `app_products`;
CREATE TABLE IF NOT EXISTS `app_products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `app_products`
--

INSERT INTO `app_products` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(1, 'smarttab-ios', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `app_products` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(2, 'smarttab-android', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `app_products` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(3, 'kiosk-ios', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `app_products` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(4, 'kiosk-android', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_logs`
--

DROP TABLE IF EXISTS `auth_logs`;
CREATE TABLE IF NOT EXISTS `auth_logs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `origin_merchant_id` int(11) NOT NULL,
  `app_id` tinyint(4) NOT NULL,
  `auth_header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `response_code` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_logs_app_id_index` (`app_id`),
  KEY `auth_logs_response_code_index` (`response_code`)
) ENGINE=InnoDB AUTO_INCREMENT=528 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `auth_logs`
--

INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(505, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-21 15:14:42', '2020-06-21 15:14:42');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(506, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 02:58:59', '2020-06-22 02:58:59');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(507, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 02:59:29', '2020-06-22 02:59:29');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(508, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:00:18', '2020-06-22 03:00:18');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(509, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:01:12', '2020-06-22 03:01:12');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(510, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:01:34', '2020-06-22 03:01:34');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(511, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:01:48', '2020-06-22 03:01:48');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(512, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:03:22', '2020-06-22 03:03:22');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(513, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:04:00', '2020-06-22 03:04:00');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(514, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:04:10', '2020-06-22 03:04:10');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(515, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:04:13', '2020-06-22 03:04:13');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(516, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:04:39', '2020-06-22 03:04:39');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(517, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:06:01', '2020-06-22 03:06:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(518, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:06:33', '2020-06-22 03:06:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(519, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:06:59', '2020-06-22 03:06:59');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(520, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:07:22', '2020-06-22 03:07:22');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(521, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:07:42', '2020-06-22 03:07:42');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(522, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:08:06', '2020-06-22 03:08:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(523, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:08:19', '2020-06-22 03:08:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(524, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:08:46', '2020-06-22 03:08:46');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(525, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:09:00', '2020-06-22 03:09:00');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(526, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:09:19', '2020-06-22 03:09:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(527, '::1', 702, 4, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2020-06-22 03:20:30', '2020-06-22 03:20:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT '0',
  `customer_id` int(11) DEFAULT '0',
  `total_amount` decimal(10,2) NOT NULL,
  `table_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msg_id` text COLLATE utf8_unicode_ci,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '1 - Success Generate, 2 - Success Reversal  3 - Sucess Refund, 4 - Success Query',
  `request` text COLLATE utf8_unicode_ci,
  `response` text COLLATE utf8_unicode_ci,
  `merchant_id` int(11) DEFAULT NULL,
  `client_info` text COLLATE utf8_unicode_ci,
  `op_type` tinyint(4) DEFAULT NULL,
  `app_id` tinyint(4) DEFAULT NULL,
  `txn_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_order_id_index` (`order_id`),
  KEY `logs_merchant_id_index` (`merchant_id`),
  KEY `logs_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(61, '2019_09_03_072530_create_apps_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(62, '2019_09_03_072621_create_app_permissions_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(63, '2019_09_03_073458_create_configs_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(64, '2019_09_03_073544_create_logs_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(65, '2019_09_03_073736_create_transactions_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(66, '2019_11_08_171826_create_auth_logs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `payment_settings`
--

DROP TABLE IF EXISTS `payment_settings`;
CREATE TABLE IF NOT EXISTS `payment_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` int(11) DEFAULT NULL,
  `restaurant_id` int(11) DEFAULT NULL,
  `payment_key` varchar(256) DEFAULT NULL,
  `payment_pass_key` varchar(256) DEFAULT NULL,
  `endpoint` varchar(145) DEFAULT NULL,
  `terminal` varchar(45) DEFAULT NULL,
  `currency` varchar(45) DEFAULT NULL,
  `parameters` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `payment_settings`
--

INSERT INTO `payment_settings` (`id`, `payment_id`, `restaurant_id`, `payment_key`, `payment_pass_key`, `endpoint`, `terminal`, `currency`, `parameters`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES(1, 59, 702, 'TabsquareAI', 'AQEthmfxKY3KYxZCw0m/n3Q5qf3VfIpPHoJfWXdT51FCam0HcLTP74PhC8uQjWp2EMFdWw2+5HzctViMSCJMYAc=-0i4u4kZEs43y2VsaU8NIZdD82enxLKrlKhMpHPNMWHo=-j#N^,7?c7V7?urE7', 'https://stag-api-adyen.tabsquare.com/v1/purchase', 'K1', '$', '[{\"name\":\"station_id\",\"value\":\"TabsquareAI_SG_POS\"},{\"name\":\"device_id\",\"value\":\"V400m-346715537\"},{\"name\":\"pos_name\",\"value\":\"TabsquareAI_SG_POS\"},{\"name\":\"api_url\",\"value\":\"https://terminal-api-test.adyen.com/sync\"},{\"name\":\"currency\",\"value\":\"SGD\"}]', '2020-06-19 14:15:46', 1, '2020-06-19 14:15:46', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions_terminal`
--

DROP TABLE IF EXISTS `transactions_terminal`;
CREATE TABLE IF NOT EXISTS `transactions_terminal` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT '0',
  `customer_id` int(11) DEFAULT '0',
  `total_amount` decimal(10,2) DEFAULT NULL,
  `table_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msg_id` text COLLATE utf8_unicode_ci,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0 - Unpaid, 1 - Paid, 2 - Reversed',
  `merchant_id` int(11) DEFAULT NULL,
  `client_info` text COLLATE utf8_unicode_ci,
  `app_id` tinyint(4) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_order_id_index` (`order_id`),
  KEY `transactions_merchant_id_index` (`merchant_id`),
  KEY `transactions_status_index` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `transactions_terminal`
--

INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(4, 440, 1, '5.30', 'K1', 'NZD', NULL, '12345678911', 0, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, NULL, NULL);
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(5, 440, 1, '5.30', 'K1', 'NZD', NULL, '123456789441', 0, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, NULL, NULL);
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(6, 440, 1, '5.30', 'K1', 'NZD', NULL, '12345678441', 0, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2019-11-14 05:53:16', '2019-11-14 05:53:16');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(7, 440, 1, '5.30', 'K1', 'NZD', NULL, '123456784441', 1, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2019-11-14 05:55:38', '2019-11-14 05:55:38');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(8, 440, 1, '5.30', 'K1', 'NZD', NULL, '123456784442', 0, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2019-11-14 06:03:41', '2019-11-14 06:03:41');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(9, 440, 1, '5.30', 'K1', 'NZD', NULL, '123456784447', 0, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2019-11-14 06:08:47', '2019-11-14 06:08:47');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(10, 440, 1, '5.30', 'K1', 'NZD', NULL, '123456784449', 1, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2019-11-14 06:09:53', '2019-11-14 06:09:53');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(11, 440, 1, '8.50', 'K1', 'NZD', NULL, 'abcd123459901', 1, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2019-11-15 06:04:56', '2019-11-15 06:04:56');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(12, 440, 1, '5.30', 'K1', 'NZD', NULL, 'abcd1234500', 0, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2019-11-25 07:54:38', '2019-11-25 07:54:38');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(13, 440, 1, '5.30', 'K1', 'NZD', NULL, '123456781', 0, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2019-11-27 03:47:19', '2019-11-27 03:47:19');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(14, 440, 1, '5.30', 'K1', 'NZD', NULL, '123456789', 0, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2019-11-27 03:55:53', '2019-11-27 03:55:53');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(15, 3702187, -1, '4.60', 'K1', 'NZD', NULL, '191209170625', 0, 2, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2020-02-26 03:56:49', '2020-02-26 03:56:49');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(16, 1234, -1, '5.60', 'K1', 'SGD', NULL, '191209170625', 0, 1, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2020-05-21 05:25:13', '2020-05-21 05:25:13');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(17, 3702187, 1, '5.33', 'K1', 'SGD', NULL, '191209170625', 0, 702, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 3, '2020-06-21 06:54:01', '2020-06-21 06:54:01');
INSERT INTO `transactions_terminal` (`id`, `order_id`, `customer_id`, `total_amount`, `table_no`, `currency_code`, `msg_id`, `txn_id`, `status`, `merchant_id`, `client_info`, `app_id`, `created_date`, `updated_date`) VALUES(18, 3702187, 1, '5.33', 'K1', 'SGD', NULL, '1209170611', 0, 702, '{\n    \"app_key\": \"9c6a6677-2d4a-473a-bb53-1df09ad032b1\"\n}', 4, '2020-06-21 09:31:15', '2020-06-21 09:31:15');

DROP TABLE IF EXISTS `users_extendeds`;
CREATE TABLE IF NOT EXISTS `users_extendeds` (
  `id` int(11) NOT NULL,
  `adyen_commission_fees` decimal(12,2) DEFAULT NULL,
  `adyen_sub_account` varchar(255) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users_extendeds`
--

INSERT INTO `users_extendeds` VALUES
(702, '18.00', '1234567', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
