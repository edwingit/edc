<?php
  
namespace App\Api\v1\Services; 

use App\Api\v1\Services\Contract\BaseWindcave;

use App\Api\v1\Services\Events\LogEvent;

use Spatie\ArrayToXml\ArrayToXml;

use Log;

class Purchase extends BaseWindcave
{
    private $payload; 
    private $unique_txn;
    private $Terminal_response;
    private $receipt;
    
    
    public function process($data)
    {
        
        $this->op_type = "PURCHASE_CREATE";

        $this->request_data = $data;

        $this->getConfig();
        
        $status = "UNDEFINED";
        
        //step 1 collect json request
        $this->setPayload("purchase");
        $this->getPayload();
        
        Log::info("Requested >>>>>>>>>>>>>>>>");
        Log::info(json_encode($this->getPayload()));
        
        
        $this->doRequest();
        
        if($this->handleResult() == false)
        {
          return false;
        }
        
        //response handler
        $this->result=$status=="APPROVED" || $status=="ACCEPTED"?true:false;
        
        $this->message_type = "Transaction : ".$status;
        
        $this->output = [
            "STAN"          =>  "",
            "MerchantId"    =>  isset($this->Terminal_response["MID"])?is_array($this->Terminal_response["MID"])?"":$this->Terminal_response["MID"]:"",
            "TerminalID"    =>  isset($this->Terminal_response["TID"])?is_array($this->Terminal_response["TID"])?"":$this->Terminal_response["TID"]:"",
            "BankDateTime"  =>  isset($this->Terminal_response["DT"])?is_array($this->Terminal_response["DT"])?"":$this->Terminal_response["DT"]:"",
            "TxnRef"        =>  isset($this->getXmlResponse()["TxnRef"])?is_array($this->getXmlResponse()["TxnRef"])?"":$this->getXmlResponse()["TxnRef"]:"",
            "CardPAN"       =>  isset($this->Terminal_response["CN"])?is_array($this->Terminal_response["CN"])?"":$this->Terminal_response["CN"]:"",
            "Receipt"       =>  "",
            "CardType"      =>  isset($this->Terminal_response["CT"])?is_array($this->Terminal_response["CT"])?"":$this->Terminal_response["CT"]:"",
            "AuthCode"      =>  isset($this->Terminal_response["AC"])?is_array($this->Terminal_response["AC"])?"":$this->Terminal_response["AC"]:"",
            "status"        =>  $status=="APPROVED"|| $status=="ACCEPTED"?"'SUCCESS'":"'FAILED'",//STATIC RESPONSE 
            "ID"            =>  "",
            "result"        =>  $status=="APPROVED"|| $status=="ACCEPTED"?true:false,
            "surcharge"     =>  0,
            "response"      =>  $this->getXmlResponse()
        ];
        
        Log::info("Response >>>>>>>>>>>>>> ");
        Log::info(json_encode($this->output));
        
        return true;
    }

    public function query()
    {
        
        $ch = curl_init($this->request_uri);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json_payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        
        $this->api_response = curl_exec($ch);
        
        
        $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
    }

    public function setPayload($type){
        $orderId = $this->request_data["order_id"];
        $this->unique_txn=$this->request_data["txn_identifier"];
        $tableNo = round($this->request_data["table_no"]);
        $amount = $this->request_data["total_amount"];
        $currency_code = $this->request_data["currency_code"];
        
        $client = new \Adyen\Client();
        
        $client->setXApiKey("AQEmhmfuXNWTK0Qc+iSEk2YrtvGaWo5sJDK+zqqik0VXZXQ8c05MtrYQwV1bDb7kfNy1WIxIIkxgBw==-Me0pisqMs/uQIbfUknHpgHbox8snsPwYsDRXrlTM8nw=-6?[bjD8Z7cL}DCr%");
        
        $client->setUsername("edwin");
        $client->setPassword("!Y3tnW3v>h]&{QKhhPmLE&ejSd");
        
        $client->setEnvironment(\Adyen\Environment::TEST);
        
        //$client->setEnvironment(\Adyen\Environment::LIVE, "Your live URL prefix"); //Live environment

        $service = new \Adyen\Service\Checkout($client);
        echo var_dump($service);die;
        
 
        $params = array(
            "amount" => array(
              "currency" => "SGD",
              "value" => 10
            ),
            "reference" => $this->request_data["txn_identifier"],
            "paymentMethod" => array(
              "type" => "scheme",
              "encryptedCardNumber" => "test_4111111111111111",
              "encryptedExpiryMonth" => "test_03",
              "encryptedExpiryYear" => "test_2030",
              "encryptedSecurityCode" => "test_737"
            ),
            "returnUrl" => "https://your-company.com/checkout?shopperOrder=12xy..",
            "merchantAccount" => "TabsquareAI_SG_POS"
          );
        
            
          $result = $service->payments($params);

        echo var_dump($result);
        die;

        
        $payload=[];
        
        date_default_timezone_set('Australia/Melbourne');
        $date = date('Y-m-d h:i:s a', time());
        
        switch($type){
            case "purchase":
                $payload=[
                    "SaleToPOIRequest"=>[
                        "MessageHeader"=>[
                          "ProtocolVersion" =>  "3.0",
                          "MessageClass"    =>  "Service",
                          "MessageCategory" =>  "Payment",
                          "MessageType"     =>  "Request",
                          //"SaleID"          =>  $this->partner_id,
                            //"ServiceID"       =>  $this->unique_txn,
                          //"POIID"           =>  $this->device_id
                            "SaleID"          =>  "POSSystemID12345",
                          "ServiceID"   =>  "0207111104",
                            "POIID" =>  "V400m-324688179"
                        ],
                        "PaymentRequest"=>[
                                "SaleData"=>[
                                    "SaleTransactionID"=>[
                                        "TransactionID" =>  $this->unique_txn,
                                        "TimeStamp"     =>  $date
                                    ]
                                ],
                                "PaymentTransaction"=>[
                                    "AmountsReq"=>[
                                        "Currency"          =>  $currency_code,
                                        "RequestedAmount"   =>  $amount
                                    ]
                                ]
                        ]
                    ]
                ];
                
                break;
            
            case "status":
                $payload=[
                    "SaleToPOIRequest"      => [
                      "MessageHeader"       => [
                        "ProtocolVersion"   => "3.0",
                        "MessageClass"      => "Service",
                        "MessageCategory"   => "TransactionStatus",
                        "MessageType"       => "Request",
                        "SaleID"            => "POSSystemID12345",
                        "ServiceID"         => "29245",
                        "POIID"             => "V400m-324688179"
                      ],
                      "TransactionStatusRequest"=> [
                        "ReceiptReprintFlag"    => true,
                        "DocumentQualifier"     => [
                          "CashierReceipt",
                          "CustomerReceipt"
                        ],
                        "MessageReference"=> [
                          "SaleID"          => "POSSystemID12345",
                          "ServiceID"       => "29235",
                          "MessageCategory" => "Payment"
                        ]
                      ]
                    ]
                    
                ];
               
                break;
            
            default :
                break;
        }
        
        $this->payload =json_encode($payload);
        $this->api_payload= json_encode($payload);
    }
    public function getPayload(){
        return json_encode($this->payload);
    }
    public function getXmlResponse(){
        $array=null;
        try{
            $xml = simplexml_load_string($this->api_response, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);
        } catch (Exception $ex) {

        }
        
        return $array;
    }
    
    public function doRequest(){
        
        $this->json_payload = $this->payload;

        $this->http_method = "POST";  

        $this->application_type = "application/json";

        $this->api_endpoint = $this->api_url;
        
        $this->request_uri = $this->api_endpoint;
        
        $this->query();
    }
    
}