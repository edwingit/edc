//
//  TyroClientUniversal.h
//  TyroClientUniversal
//
//  Created by development on 28/10/2014.
//  Copyright (c) 2014 Tyro Payments. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TyroClientUniversal.
FOUNDATION_EXPORT double TyroClientUniversalVersionNumber;

//! Project version string for TyroClientUniversal.
FOUNDATION_EXPORT const unsigned char TyroClientUniversalVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TyroClientUniversal/PublicHeader.h>

#import <TyroClientUniversal/TyroClient.h>


