/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

@import UIKit;

typedef void (^TransactionCompleteCallback)(NSDictionary *result);

typedef void (^ReceiptReceivedCallback)(NSDictionary *result);

/*!
* @brief The interface for interacting with a Tyro terminal.
*/
@interface TyroClient : NSObject

/*!
* @brief Replace <i>https://iclient.tyro.com</i> with one of the following URLs for development/testing:
* <ul>
* <li><i>https://iclientsimulator.test.tyro.com</i> connects to the integration server simulator which does not require a physical payment terminal</li>
* <li><i>https://iclient.test.tyro.com</i> connects a test system that can only be used with a physical terminal</li>
* </ul>
*/
@property(nonatomic, strong) NSString *baseUrl;

/*!
* @brief Initialises the given webview with the iClient.
*
* @param webView The UIWebView to be used for display.
* @param webViewDelegate The delegate to be used for the webView.
* @param apiKey The API key provided by Tyro.
* @param posProductVendor Name of your company.
* @param posProductName Name of your product.
* @param posProductVersion Version of your product.
*/
- (instancetype)initWithWebView:(UIWebView *)webView
                webViewDelegate:(id <UIWebViewDelegate>)webViewDelegate
                         apiKey:(NSString *)apiKey
               posProductVendor:(NSString *)posProductVendor
                 posProductName:(NSString *)posProductName
              posProductVersion:(   NSString *)posProductVersion;

/*!
* @brief Starts the pairing process in the webview provided during initWithWebView.
*/
- (void)pairTerminal;

/*!
 * @brief Starts the upload logs process in the webview provided during initWithWebView.
 */
- (void)uploadLogs;

/*!
* @brief Tell the terminal to perform the given operation
*
* For a full set of operations and parameters refer to the iClient documentations
*
* @param operation The name of the operation to perform (i.e. purchase, refund etc..)
* @param parameters A dictionary containing the parameters that will be used in the request
* @param receiptReceivedCallback The callback that will be triggered whenever a new receipt is received during the transaction.
* @param transactionCompleteCallback The callback that will be triggered when the transaction is completed.
*/
- (void)performOperation:(NSString *)operation
              parameters:(NSDictionary *)parameters
    receiptReceivedCallback:(ReceiptReceivedCallback)receiptReceivedCallback
transactionCompleteCallback:(TransactionCompleteCallback)transactionCompleteCallback;

@end
