/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

#import <TyroClientUniversal/TyroClient.h>
#import "HealthPointViewController.h"
#import "PosConfiguration.h"

@interface HealthPointViewController ()

@property(nonatomic, strong) PosConfiguration *posConfiguration;
@property(nonatomic, strong) TyroClient *client;
@property (weak, nonatomic) IBOutlet UIWebView *mainWebView;
@property (weak, nonatomic) IBOutlet UITextField *claimAmountField;
@property (weak, nonatomic) IBOutlet UITextField *itemCodeField;
@property (weak, nonatomic) IBOutlet UITextField *itemDescriptionField;
@property (weak, nonatomic) IBOutlet UITextField *serviceReferenceField;
@property (weak, nonatomic) IBOutlet UITextField *patientIdField;
@property (weak, nonatomic) IBOutlet UITextField *serviceDateField;
@property (weak, nonatomic) IBOutlet UITextField *serviceTypeField;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UITextView *dataField;
@property (weak, nonatomic) IBOutlet UITextField *refTagField;

@end

@implementation HealthPointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.posConfiguration = [PosConfiguration sharedInstance];
    self.client = [[TyroClient alloc] initWithWebView:self.mainWebView
                                      webViewDelegate:self
                                               apiKey:self.posConfiguration.apiKey
                                     posProductVendor:self.posConfiguration.posProductVendor
                                       posProductName:self.posConfiguration.posProductName
                                    posProductVersion:self.posConfiguration.posProductVersion];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.client.baseUrl = self.posConfiguration.iclientBaseUrl;
}

- (IBAction)initiateClaim {
    [self.claimAmountField resignFirstResponder];
    self.resultLabel.text = @"";
    
    [self.client performOperation:@"healthpointClaim"
                       parameters:@{@"providerId" : self.posConfiguration.providerNumber,
                                    @"serviceType" : self.serviceTypeField.text,
                                    @"claimItemsCount" : @"1",
                                    @"totalClaimAmount" : self.claimAmountField.text,
                                    @"claimItems": @[
                                            @{@"claimAmount": self.claimAmountField.text,
                                              @"serviceCode": self.itemCodeField.text,
                                              @"description": self.itemDescriptionField.text,
                                              @"serviceReference": self.serviceReferenceField.text,
                                              @"patientId": self.patientIdField.text,
                                              @"serviceDate": self.serviceDateField.text}
                                    ]
                                    }
          receiptReceivedCallback:nil
      transactionCompleteCallback:[self transactionCompleteCallback]];
}

- (IBAction)cancelClaim:(id)sender {
    [self.refTagField resignFirstResponder];
    self.resultLabel.text = @"";
    
    [self.client performOperation:@"healthpointCancel"
                       parameters:@{@"providerId" : self.posConfiguration.providerNumber,
                                    @"serviceType" : self.serviceTypeField.text,
                                    @"claimItemsCount" : @"1",
                                    @"totalClaimAmount" : self.claimAmountField.text,
                                    @"claimItems": @[
                                            @{@"claimAmount": self.claimAmountField.text,
                                              @"serviceCode": self.itemCodeField.text,
                                              @"description": self.itemDescriptionField.text,
                                              @"serviceReference": self.serviceReferenceField.text,
                                              @"patientId": self.patientIdField.text,
                                              @"serviceDate": self.serviceDateField.text}
                                            ],
                                    @"refTag": self.refTagField.text
                                    }
          receiptReceivedCallback:nil
      transactionCompleteCallback:[self transactionCompleteCallback]];
}

- (void (^)(NSDictionary *))transactionCompleteCallback {
    return ^(NSDictionary *result) {
        NSLog(@"Transaction complete result: %@", result);
        self.resultLabel.text = [@"Result: " stringByAppendingString:result[@"result"]];
        self.dataField.text = result[@"data"];
    };
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"Start loading web view");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"Finish loading web view");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Failed to start transaction"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
