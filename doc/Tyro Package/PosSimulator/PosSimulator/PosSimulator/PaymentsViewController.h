/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

@interface PaymentsViewController : UIViewController <UIWebViewDelegate>

@end
