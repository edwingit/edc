/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

#import <TyroClientUniversal/TyroClient.h>
#import "MedicareViewController.h"
#import "PosConfiguration.h"

@interface MedicareViewController ()

@property(nonatomic, strong) PosConfiguration *posConfiguration;
@property(nonatomic, strong) TyroClient *client;

@property (weak, nonatomic) IBOutlet UILabel *resultsLabel;
@property (weak, nonatomic) IBOutlet UIWebView *mainWebView;
@property (weak, nonatomic) IBOutlet UITextField *fullyPaidAmountField;
@property (weak, nonatomic) IBOutlet UITextField *partPaidAmountField;
@property (weak, nonatomic) IBOutlet UITextField *partPaidContributionAmountField;

@end

@implementation MedicareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.posConfiguration = [PosConfiguration sharedInstance];
    self.client = [[TyroClient alloc] initWithWebView:self.mainWebView
                                      webViewDelegate:self
                                               apiKey:self.posConfiguration.apiKey
                                     posProductVendor:self.posConfiguration.posProductVendor
                                       posProductName:self.posConfiguration.posProductName
                                    posProductVersion:self.posConfiguration.posProductVersion];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.client.baseUrl = self.posConfiguration.iclientBaseUrl;
}

- (IBAction)initiateFullyPaidClaim:(UIButton *)button {
    [self.fullyPaidAmountField resignFirstResponder];
    self.resultsLabel.text = @"";

    NSString *payload = [self fullyPaidPayloadWithAmount:self.fullyPaidAmountField.text];

    [self.client performOperation:@"easyclaim"
                       parameters:@{@"type" : @"fullypaid",
                                    @"rightsAssigned" : @"true",
                                    @"payload" : payload}
          receiptReceivedCallback:nil
      transactionCompleteCallback:[self transactionCompleteCallback]];
}

- (IBAction)initiatePartPaidClaim:(UIButton *)button {
    [self.partPaidAmountField resignFirstResponder];
    self.resultsLabel.text = @"";

    NSString *payload = [self partPaidPayloadWithAmount:self.partPaidAmountField.text contributionAmount:self.partPaidContributionAmountField.text];

    [self.client performOperation:@"easyclaim"
                       parameters:@{@"type" : @"partpaid",
                                    @"rightsAssigned" : @"true",
                                    @"payload" : payload}
          receiptReceivedCallback:nil
      transactionCompleteCallback:[self transactionCompleteCallback]];
}

- (IBAction)initiateBulkBillClaim:(UIButton *)button {
    self.resultsLabel.text = @"";
    
    NSString *payload = [self bulkBillPayload];
    
    [self.client performOperation:@"easyclaim"
                       parameters:@{@"type" : @"bulkbill",
                                    @"rightsAssigned" : @"true",
                                    @"payload" : payload}
          receiptReceivedCallback:nil
      transactionCompleteCallback:[self transactionCompleteCallback]];
}

- (NSString *)fullyPaidPayloadWithAmount:(NSString *)amount {
    return [NSString stringWithFormat:@"<ns1:patienteClaimingRequest xmlns:ns1=\"http://medicareaustralia.gov.au/eclaiming/version-2\"><claim accountReferenceNum=\"123456789\" accountPaidInd=\"Y\"><voucher voucherId=\"01\" serviceTypeCde=\"O\"><service lspnNum=\"1\" dateOfService=\"2014-09-01+10:00\" itemOverrideCde=\"AP\" mbsItemNum=\"23\" chargeAmount=\"%@\" serviceId=\"0001\"/></voucher><patient memberNum=\"2298467221\" memberRefNum=\"1\"/><servicingProvider providerNum=\"2147661H\"/></claim></ns1:patienteClaimingRequest>",
                    amount];
}

- (NSString *)partPaidPayloadWithAmount:(NSString *)amount
                     contributionAmount:(NSString *)contributionAmount {
    return [NSString stringWithFormat:@"<ns1:patienteClaimingRequest xmlns:ns1=\"http://medicareaustralia.gov.au/eclaiming/version-2\"><claim accountReferenceNum=\"123456789\" accountPaidInd=\"N\"><voucher voucherId=\"01\" serviceTypeCde=\"O\"><service lspnNum=\"1\" dateOfService=\"2014-09-01+10:00\" itemOverrideCde=\"AP\" mbsItemNum=\"23\" chargeAmount=\"%@\" patientContribAmt=\"%@\" serviceId=\"0001\"/></voucher><patient memberNum=\"2298467221\" memberRefNum=\"1\"/><servicingProvider providerNum=\"2147661H\"/></claim></ns1:patienteClaimingRequest>", amount, contributionAmount];
}

- (NSString *)bulkBillPayload {
    return @"<ns1:bulkBilleClaimingRequest xmlns:ns1=\"http://medicareaustralia.gov.au/eclaiming/version-2\"><claim cevRequestInd=\"Y\"><voucher voucherId=\"01\" serviceTypeCde=\"O\"><service mbsItemNum=\"23\" dateOfService=\"2014-09-02+10:00\" serviceId=\"0001\"/></voucher><patient memberNum=\"2298467221\" memberRefNum=\"1\"/><servicingProvider providerNum=\"2147661H\"/></claim></ns1:bulkBilleClaimingRequest>";
}

- (void (^)(NSDictionary *))transactionCompleteCallback {
    return ^(NSDictionary *result) {
        NSLog(@"Transaction complete result: %@", result);
        self.resultsLabel.text = [@"Result: " stringByAppendingString:result[@"result"]];
    };
}


@end
