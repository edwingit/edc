//
//  PosSimulatorAppDelegate.h
//  PosSimulator
//
//  Created by development on 17/07/2014.
//  Copyright (c) 2014 Tyro Payment Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PosSimulatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
