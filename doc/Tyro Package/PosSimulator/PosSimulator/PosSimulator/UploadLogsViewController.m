/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

#import "UploadLogsViewController.h"
#import <TyroClientUniversal/TyroClient.h>
#import "PosConfiguration.h"

@interface UploadLogsViewController ()

@property(weak, nonatomic) IBOutlet UIWebView *uploadLogsWebView;
@property(strong, nonatomic) PosConfiguration *posConfiguration;
@property(strong, nonatomic) TyroClient *client;

@end

@implementation UploadLogsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.posConfiguration = [PosConfiguration sharedInstance];

    self.client = [[TyroClient alloc] initWithWebView:self.uploadLogsWebView
                                      webViewDelegate:self
                                               apiKey:self.posConfiguration.apiKey
                                     posProductVendor:self.posConfiguration.posProductVendor
                                       posProductName:self.posConfiguration.posProductName
                                    posProductVersion:self.posConfiguration.posProductVersion];
    self.client.baseUrl = self.posConfiguration.iclientBaseUrl;
}

- (void)viewDidAppear:(BOOL)animated {
    [self.client uploadLogs];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"Start loading web view");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"Finish loading web view");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Failed to load"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
