/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

#import <TyroClientUniversal/TyroClient.h>
#import "PaymentsViewController.h"
#import "PosConfiguration.h"

@interface PaymentsViewController ()

@property(weak, nonatomic) IBOutlet UITextField *amountField;
@property(weak, nonatomic) IBOutlet UITextField *cashoutAmountField;
@property(weak, nonatomic) IBOutlet UITextField *refundAmountField;
@property(weak, nonatomic) IBOutlet UITextField *openTabAmountField;
@property(weak, nonatomic) IBOutlet UITextField *closeTabAmountField;
@property(weak, nonatomic) IBOutlet UITextField *closeTabCompletionReferenceField;
@property(weak, nonatomic) IBOutlet UITextField *voidTabCompletionReferenceField;

@property(strong, nonatomic) IBOutlet UIWebView *mainWebView;
@property(weak, nonatomic) IBOutlet UITextView *merchantReceiptTextView;
@property(weak, nonatomic) IBOutlet UITextView *customerReceiptTextView;

@property(nonatomic, strong) PosConfiguration *posConfiguration;
@property(nonatomic, strong) TyroClient *client;

@end

@implementation PaymentsViewController

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.posConfiguration = [PosConfiguration sharedInstance];
    self.mainWebView = [[UIWebView alloc] initWithFrame:self.mainWebView.frame];
    [self.view addSubview:self.mainWebView];

    self.client = [[TyroClient alloc] initWithWebView:self.mainWebView
                                      webViewDelegate:self
                                               apiKey:self.posConfiguration.apiKey
                                     posProductVendor:self.posConfiguration.posProductVendor
                                       posProductName:self.posConfiguration.posProductName
                                    posProductVersion:self.posConfiguration.posProductVersion];
    
    self.client.baseUrl = self.posConfiguration.iclientBaseUrl;
}

- (IBAction)initiatePurchase {
    [self.amountField resignFirstResponder];
    [self clearReceipts];

    NSDictionary *parameters = @{
            @"amount" : self.amountField.text,
            @"cashout" : self.cashoutAmountField.text,
            @"integratedReceipt" : self.posConfiguration.integratedReceipt ? @"true" : @"false"};

    if ([self.amountField.text isEqualToString:@"error"]) {
        [self.client performOperation:@"invalid-operation"
                           parameters:parameters
              receiptReceivedCallback:[self receiptReceivedCallback]
          transactionCompleteCallback:[self transactionCompleteCallback]];
    } else {
        [self.client performOperation:@"purchase"
                           parameters:parameters
              receiptReceivedCallback:[self receiptReceivedCallback]
          transactionCompleteCallback:[self transactionCompleteCallback]];
    }
}

- (IBAction)initiateRefund {
    [self.refundAmountField resignFirstResponder];
    [self clearReceipts];

    [self.client performOperation:@"refund"
                       parameters:@{@"amount" : self.refundAmountField.text,
                               @"integratedReceipt" : self.posConfiguration.integratedReceipt ? @"true" : @"false"}
          receiptReceivedCallback:[self receiptReceivedCallback]
      transactionCompleteCallback:[self transactionCompleteCallback]];
}

- (IBAction)initiateOpenTab {
    [self.openTabAmountField resignFirstResponder];
    [self clearReceipts];

    [self.client performOperation:@"openTab"
                       parameters:@{@"amount" : self.openTabAmountField.text,
                               @"integratedReceipt" : self.posConfiguration.integratedReceipt ? @"true" : @"false"}
          receiptReceivedCallback:[self receiptReceivedCallback]
      transactionCompleteCallback:[self transactionCompleteCallback]];
}

- (IBAction)closeTab {
    [self.closeTabAmountField resignFirstResponder];
    [self clearReceipts];

    [self.client performOperation:@"closeTab"
                       parameters:@{@"amount" : self.closeTabAmountField.text,
                               @"completionReference" : self.closeTabCompletionReferenceField.text}
          receiptReceivedCallback:[self receiptReceivedCallback]
      transactionCompleteCallback:[self transactionCompleteWithAlertCallback]];
}

- (IBAction)voidTab {
    [self.voidTabCompletionReferenceField resignFirstResponder];
    [self clearReceipts];

    [self.client performOperation:@"voidTab"
                       parameters:@{@"completionReference" : self.voidTabCompletionReferenceField.text}
          receiptReceivedCallback:[self receiptReceivedCallback]
      transactionCompleteCallback:[self transactionCompleteWithAlertCallback]];
}

- (void (^)(NSDictionary *))receiptReceivedCallback {
    return ^(NSDictionary *receipt) {
        NSLog(@"%@", receipt);
        NSLog(@"merchant receipt = %@", receipt[@"merchantReceipt"]);
        self.merchantReceiptTextView.text = receipt[@"merchantReceipt"];
    };
}

- (void (^)(NSDictionary *))transactionCompleteCallback {
    return ^(NSDictionary *result) {
        NSString *transactionResult = result[@"result"];
        NSLog(@"Transaction complete result: %@", result);

        if ([transactionResult isEqualToString:@"SYSTEM ERROR"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:transactionResult
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        } else {
            self.customerReceiptTextView.text = result[@"customerReceipt"];
        }

    };
}

- (void (^)(NSDictionary *))transactionCompleteWithAlertCallback {
    return ^(NSDictionary *result) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Close Tab"
                                                        message:result[@"message"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
}

- (void)clearReceipts {
    self.merchantReceiptTextView.text = @"";
    self.customerReceiptTextView.text = @"";
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"Start loading web view");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"Finish loading web view");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Failed to start transaction"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
