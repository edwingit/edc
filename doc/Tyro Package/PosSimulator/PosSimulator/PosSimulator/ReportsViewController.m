/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

#import <TyroClientUniversal/TyroClient.h>
#import "ReportsViewController.h"
#import "PosConfiguration.h"

@interface ReportsViewController ()

@property(nonatomic, strong) PosConfiguration *posConfiguration;
@property(nonatomic, strong) TyroClient *client;

@property (weak, nonatomic) IBOutlet UITextField *paymentReportDateField;
@property (weak, nonatomic) IBOutlet UITextField *healthPointReportDateField;
@property (weak, nonatomic) IBOutlet UILabel *resultsLabel;
@property (weak, nonatomic) IBOutlet UIWebView *mainWebView;
@property (weak, nonatomic) IBOutlet UITextView *reportView;

@end

@implementation ReportsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.posConfiguration = [PosConfiguration sharedInstance];
    self.client = [[TyroClient alloc] initWithWebView:self.mainWebView
                                      webViewDelegate:self
                                               apiKey:self.posConfiguration.apiKey
                                     posProductVendor:self.posConfiguration.posProductVendor
                                       posProductName:self.posConfiguration.posProductName
                                    posProductVersion:self.posConfiguration.posProductVersion];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.client.baseUrl = self.posConfiguration.iclientBaseUrl;
}

- (IBAction)paymentsReconciliationReport:(UIButton *)button {
    [self.paymentReportDateField resignFirstResponder];
    self.resultsLabel.text = @"";
    
    NSString *reportType = [button.titleLabel text];
    
    [self.client performOperation:@"reconciliationReport"
                       parameters:@{@"terminalBusinessDay" : self.paymentReportDateField.text,
                                    @"reportFormat" : @"txt",
                                    @"reportType" : reportType}
          receiptReceivedCallback:nil
      transactionCompleteCallback:[self reportCompleteCallback]];
}

- (IBAction)healthPointReport:(UIButton *)button {
    [self.healthPointReportDateField resignFirstResponder];
    self.resultsLabel.text = @"";
    
    NSString *reportType = [[button.titleLabel text] lowercaseString];
    
    [self.client performOperation:@"healthpointReconciliationReport"
                       parameters:@{@"reconDate" : self.healthPointReportDateField.text,
                                    @"reportType" : reportType}
          receiptReceivedCallback:nil
      transactionCompleteCallback:[self reportCompleteCallback]];
}

- (IBAction)manualSettlement:(UIButton *)button {
    self.resultsLabel.text = @"";
    
    [self.client performOperation:@"manualSettlement"
                       parameters:nil
          receiptReceivedCallback:nil
      transactionCompleteCallback:[self manualSettlementCompleteCallback]];
}

- (void (^)(NSDictionary *))reportCompleteCallback {
    return ^(NSDictionary *result) {
        NSLog(@"Transaction complete result: %@", result);
        self.resultsLabel.text = [@"Result: " stringByAppendingString:result[@"result"]];
        self.reportView.text = result[@"data"];
    };
}

- (void (^)(NSDictionary *))manualSettlementCompleteCallback {
    return ^(NSDictionary *result) {
        NSLog(@"Transaction complete result: %@", result);
        self.resultsLabel.text = [@"Result: " stringByAppendingString:result[@"result"]];
        self.reportView.text = result[@"message"];
    };
}

@end
