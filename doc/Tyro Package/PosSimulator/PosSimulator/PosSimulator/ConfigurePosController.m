/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

#import <TyroClientUniversal/TyroClient.h>
#import "ConfigurePosController.h"
#import "PosConfiguration.h"

@interface ConfigurePosController ()

@property(weak, nonatomic) IBOutlet UIWebView *configurationWebView;
@property(weak, nonatomic) IBOutlet UISwitch *integratedReceiptSwitch;
@property(weak, nonatomic) IBOutlet UITextField *baseUrlField;
@property(weak, nonatomic) IBOutlet UITextField *providerNumberField;
@property (weak, nonatomic) IBOutlet UITextField *apiKeyField;

@property(strong, nonatomic) PosConfiguration *posConfiguration;
@property(strong, nonatomic) TyroClient *client;

@end

@implementation ConfigurePosController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.posConfiguration = [PosConfiguration sharedInstance];
    self.integratedReceiptSwitch.on = self.posConfiguration.integratedReceipt;
    self.baseUrlField.text = self.posConfiguration.iclientBaseUrl;
    self.providerNumberField.text = self.posConfiguration.providerNumber;

    self.client = [[TyroClient alloc] initWithWebView:self.configurationWebView
                                      webViewDelegate:self
                                               apiKey:self.posConfiguration.apiKey
                                     posProductVendor:self.posConfiguration.posProductVendor
                                       posProductName:self.posConfiguration.posProductName
                                    posProductVersion:self.posConfiguration.posProductVersion];
    self.client.baseUrl = self.posConfiguration.iclientBaseUrl;
}

- (void)viewDidAppear:(BOOL)animated {
    [self.client pairTerminal];
}

- (IBAction)refreshScreen:(id)sender {
    [self.baseUrlField resignFirstResponder];
    [self.apiKeyField resignFirstResponder];
    [self.client pairTerminal];
}

- (IBAction)integratedReceiptChanged:(id)sender {
    self.posConfiguration.integratedReceipt = [self.integratedReceiptSwitch isOn];
}

- (IBAction)baseUrlChanged:(id)sender {
    self.posConfiguration.iclientBaseUrl = self.baseUrlField.text;
    self.client.baseUrl = self.posConfiguration.iclientBaseUrl;
}
- (IBAction)apiKeyChanged:(id)sender {
    self.posConfiguration.apiKey = self.apiKeyField.text;
    self.apiKeyField.text = @"";
}

- (IBAction)providerNumberChanged:(id)sender {
    self.posConfiguration.providerNumber = self.providerNumberField.text;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"Start loading web view");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"Finish loading web view");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Failed to load"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
