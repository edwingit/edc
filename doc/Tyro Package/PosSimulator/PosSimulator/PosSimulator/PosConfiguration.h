/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

@interface PosConfiguration : NSObject
@property(nonatomic, strong) NSString *posProductVendor;
@property(nonatomic, strong) NSString *posProductName;
@property(nonatomic, strong) NSString *posProductVersion;
@property(nonatomic, strong) NSString *apiKey;
@property(nonatomic) BOOL integratedReceipt;
@property(nonatomic, strong) NSString *iclientBaseUrl;
@property(nonatomic, strong) NSString *providerNumber;

+ (PosConfiguration *)sharedInstance;
@end