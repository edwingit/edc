/*
* Copyright (c) 2003 - 2014 Tyro Payments Limited.
* 125 York St, Sydney NSW 2000.
* All rights reserved.
*/

#import "PosConfiguration.h"

@implementation PosConfiguration

+ (PosConfiguration *)sharedInstance {
    static PosConfiguration *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PosConfiguration alloc] init];
        sharedInstance.apiKey = @"111";
        sharedInstance.posProductVendor = @"Tyro";
        sharedInstance.posProductName = @"Pos Simulator";
        sharedInstance.posProductVersion = @"1";
        sharedInstance.integratedReceipt = YES;
        sharedInstance.iclientBaseUrl = @"https://iclientsimulator.test.tyro.com";
        sharedInstance.providerNumber = @"2147661H";
    });
    return sharedInstance;
}

@end