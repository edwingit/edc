//
//  main.m
//  PosSimulator
//
//  Created by development on 17/07/2014.
//  Copyright (c) 2014 Tyro Payment Limited. All rights reserved.
//

#import "PosSimulatorAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PosSimulatorAppDelegate class]));
    }
}
