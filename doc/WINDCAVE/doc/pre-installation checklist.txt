Please ensure you have the following organised and ready: 
• A Pinpad and other necessary cables and components (as listed in this document) 
• Payline username and password to access Payment Express online tools and services 
• Merchant number and/or Terminal ID from your bank which is setup specifically for EFTPOS 
• Receipt Printer if using IPP350, if using IWL250 or IWL252 on-board printer can be used 
• A stable Broadband Internet Connection 
