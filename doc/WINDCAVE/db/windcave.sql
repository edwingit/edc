-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 10 Nov 2019 pada 12.30
-- Versi server: 5.6.17
-- Versi PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `windcave`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `apps`
--

DROP TABLE IF EXISTS `apps`;
CREATE TABLE IF NOT EXISTS `apps` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `apps`
--

INSERT INTO `apps` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(1, 'smarttab-ios', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `apps` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(2, 'smarttab-android', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `apps` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(3, 'kiosk-ios', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `apps` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES(4, 'kiosk-android', 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_permissions`
--

DROP TABLE IF EXISTS `app_permissions`;
CREATE TABLE IF NOT EXISTS `app_permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `app_id` int(11) NOT NULL,
  `app_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app_permissions_app_id_index` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `app_permissions`
--

INSERT INTO `app_permissions` (`id`, `app_id`, `app_key`, `app_secret`, `is_active`, `created_at`, `updated_at`) VALUES(1, 3, '9c6a6677-2d4a-473a-bb53-1df09ad032b1', 'e1ded469-654d-42e9-f546-de521c60bc30', 1, '2019-09-04 00:00:00', '2019-09-04 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_logs`
--

DROP TABLE IF EXISTS `auth_logs`;
CREATE TABLE IF NOT EXISTS `auth_logs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `origin_merchant_id` int(11) NOT NULL,
  `app_id` tinyint(4) NOT NULL,
  `auth_header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `response_code` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_logs_app_id_index` (`app_id`),
  KEY `auth_logs_response_code_index` (`response_code`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `auth_logs`
--

INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(1, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:50:20', '2019-11-09 00:50:20');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(2, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:50:57', '2019-11-09 00:50:57');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(3, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:51:52', '2019-11-09 00:51:52');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(4, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:52:06', '2019-11-09 00:52:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(5, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:55:25', '2019-11-09 00:55:25');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(6, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:55:54', '2019-11-09 00:55:54');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(7, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:56:28', '2019-11-09 00:56:28');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(8, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:57:15', '2019-11-09 00:57:15');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(9, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:57:19', '2019-11-09 00:57:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(10, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:58:06', '2019-11-09 00:58:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(11, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:58:46', '2019-11-09 00:58:46');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(12, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:58:57', '2019-11-09 00:58:57');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(13, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 00:59:21', '2019-11-09 00:59:21');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(14, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:02:27', '2019-11-09 01:02:27');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(15, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:12:06', '2019-11-09 01:12:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(16, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:16:29', '2019-11-09 01:16:29');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(17, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:16:45', '2019-11-09 01:16:45');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(18, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:17:01', '2019-11-09 01:17:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(19, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:17:33', '2019-11-09 01:17:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(20, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:18:05', '2019-11-09 01:18:05');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(21, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:19:02', '2019-11-09 01:19:02');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(22, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:20:12', '2019-11-09 01:20:12');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(23, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:21:48', '2019-11-09 01:21:48');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(24, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:27:56', '2019-11-09 01:27:56');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(25, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:28:46', '2019-11-09 01:28:46');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(26, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:28:58', '2019-11-09 01:28:58');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(27, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:29:27', '2019-11-09 01:29:27');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(28, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:29:46', '2019-11-09 01:29:46');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(29, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:30:14', '2019-11-09 01:30:14');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(30, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:34:23', '2019-11-09 01:34:23');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(31, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:34:41', '2019-11-09 01:34:41');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(32, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:35:04', '2019-11-09 01:35:04');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(33, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:37:29', '2019-11-09 01:37:29');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(34, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:38:35', '2019-11-09 01:38:35');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(35, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:39:12', '2019-11-09 01:39:12');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(36, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:39:24', '2019-11-09 01:39:24');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(37, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:39:47', '2019-11-09 01:39:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(38, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:40:57', '2019-11-09 01:40:57');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(39, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:42:47', '2019-11-09 01:42:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(40, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:45:40', '2019-11-09 01:45:40');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(41, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:46:39', '2019-11-09 01:46:39');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(42, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:50:58', '2019-11-09 01:50:58');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(43, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:51:36', '2019-11-09 01:51:36');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(44, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:52:05', '2019-11-09 01:52:05');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(45, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:52:38', '2019-11-09 01:52:38');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(46, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:52:52', '2019-11-09 01:52:52');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(47, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:53:16', '2019-11-09 01:53:16');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(48, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:53:39', '2019-11-09 01:53:39');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(49, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:54:09', '2019-11-09 01:54:09');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(50, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:54:50', '2019-11-09 01:54:50');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(51, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 01:56:05', '2019-11-09 01:56:05');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(52, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:02:13', '2019-11-09 02:02:13');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(53, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:02:37', '2019-11-09 02:02:37');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(54, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:05:41', '2019-11-09 02:05:41');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(55, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:05:56', '2019-11-09 02:05:56');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(56, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:06:39', '2019-11-09 02:06:39');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(57, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:06:49', '2019-11-09 02:06:49');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(58, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:07:24', '2019-11-09 02:07:24');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(59, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:08:42', '2019-11-09 02:08:42');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(60, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:09:05', '2019-11-09 02:09:05');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(61, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:09:35', '2019-11-09 02:09:35');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(62, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:11:22', '2019-11-09 02:11:22');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(63, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:11:35', '2019-11-09 02:11:35');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(64, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:11:50', '2019-11-09 02:11:50');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(65, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:13:14', '2019-11-09 02:13:14');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(66, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:13:26', '2019-11-09 02:13:26');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(67, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:13:49', '2019-11-09 02:13:49');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(68, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:17:31', '2019-11-09 02:17:31');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(69, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:19:31', '2019-11-09 02:19:31');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(70, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:20:20', '2019-11-09 02:20:20');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(71, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:20:42', '2019-11-09 02:20:42');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(72, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:23:19', '2019-11-09 02:23:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(73, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:25:34', '2019-11-09 02:25:34');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(74, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:26:26', '2019-11-09 02:26:26');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(75, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:32:04', '2019-11-09 02:32:04');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(76, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:33:16', '2019-11-09 02:33:16');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(77, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:34:00', '2019-11-09 02:34:00');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(78, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 02:43:28', '2019-11-09 02:43:28');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(79, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:10:27', '2019-11-09 08:10:27');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(80, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:11:13', '2019-11-09 08:11:13');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(81, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:19:07', '2019-11-09 08:19:07');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(82, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:21:25', '2019-11-09 08:21:25');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(83, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:23:36', '2019-11-09 08:23:36');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(84, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:23:51', '2019-11-09 08:23:51');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(85, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:24:05', '2019-11-09 08:24:05');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(86, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:38:45', '2019-11-09 08:38:45');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(87, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:39:32', '2019-11-09 08:39:32');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(88, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:39:51', '2019-11-09 08:39:51');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(89, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:40:44', '2019-11-09 08:40:44');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(90, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:42:14', '2019-11-09 08:42:14');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(91, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:48:30', '2019-11-09 08:48:30');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(92, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:48:41', '2019-11-09 08:48:41');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(93, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 08:48:51', '2019-11-09 08:48:51');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(94, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:37:15', '2019-11-09 23:37:15');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(95, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:50:53', '2019-11-09 23:50:53');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(96, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:51:26', '2019-11-09 23:51:26');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(97, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:53:18', '2019-11-09 23:53:18');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(98, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:56:06', '2019-11-09 23:56:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(99, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:56:26', '2019-11-09 23:56:26');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(100, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:57:31', '2019-11-09 23:57:31');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(101, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:58:12', '2019-11-09 23:58:12');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(102, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:59:18', '2019-11-09 23:59:18');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(103, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-09 23:59:51', '2019-11-09 23:59:51');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(104, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:11:52', '2019-11-10 00:11:52');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(105, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:12:06', '2019-11-10 00:12:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(106, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:14:03', '2019-11-10 00:14:03');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(107, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:15:49', '2019-11-10 00:15:49');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(108, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:16:06', '2019-11-10 00:16:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(109, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:16:12', '2019-11-10 00:16:12');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(110, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:19:04', '2019-11-10 00:19:04');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(111, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:19:18', '2019-11-10 00:19:18');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(112, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:19:48', '2019-11-10 00:19:48');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(113, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:20:39', '2019-11-10 00:20:39');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(114, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:21:38', '2019-11-10 00:21:38');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(115, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:22:16', '2019-11-10 00:22:16');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(116, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:23:53', '2019-11-10 00:23:53');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(117, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:24:20', '2019-11-10 00:24:20');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(118, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:25:33', '2019-11-10 00:25:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(119, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:27:39', '2019-11-10 00:27:39');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(120, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:28:33', '2019-11-10 00:28:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(121, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:30:48', '2019-11-10 00:30:48');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(122, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:32:11', '2019-11-10 00:32:11');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(123, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:32:23', '2019-11-10 00:32:23');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(124, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 00:33:10', '2019-11-10 00:33:10');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(125, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:21:20', '2019-11-10 01:21:20');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(126, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:22:13', '2019-11-10 01:22:13');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(127, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:25:20', '2019-11-10 01:25:20');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(128, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:27:27', '2019-11-10 01:27:27');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(129, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:37:40', '2019-11-10 01:37:40');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(130, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:38:55', '2019-11-10 01:38:55');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(131, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:42:12', '2019-11-10 01:42:12');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(132, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:43:47', '2019-11-10 01:43:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(133, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:44:44', '2019-11-10 01:44:44');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(134, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:46:03', '2019-11-10 01:46:03');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(135, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:53:19', '2019-11-10 01:53:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(136, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:54:06', '2019-11-10 01:54:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(137, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:55:11', '2019-11-10 01:55:11');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(138, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:56:03', '2019-11-10 01:56:03');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(139, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:56:37', '2019-11-10 01:56:37');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(140, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:58:23', '2019-11-10 01:58:23');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(141, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 01:59:19', '2019-11-10 01:59:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(142, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:00:27', '2019-11-10 02:00:27');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(143, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:00:51', '2019-11-10 02:00:51');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(144, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:02:05', '2019-11-10 02:02:05');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(145, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:03:13', '2019-11-10 02:03:13');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(146, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:04:08', '2019-11-10 02:04:08');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(147, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:04:41', '2019-11-10 02:04:41');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(148, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:06:33', '2019-11-10 02:06:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(149, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:07:10', '2019-11-10 02:07:10');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(150, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:07:27', '2019-11-10 02:07:27');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(151, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:07:40', '2019-11-10 02:07:40');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(152, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:08:23', '2019-11-10 02:08:23');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(153, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:09:16', '2019-11-10 02:09:16');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(154, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:31:32', '2019-11-10 02:31:32');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(155, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:32:16', '2019-11-10 02:32:16');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(156, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:33:49', '2019-11-10 02:33:49');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(157, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:43:50', '2019-11-10 02:43:50');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(158, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:44:26', '2019-11-10 02:44:26');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(159, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:47:41', '2019-11-10 02:47:41');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(160, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:52:51', '2019-11-10 02:52:51');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(161, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 02:56:36', '2019-11-10 02:56:36');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(162, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:00:25', '2019-11-10 03:00:25');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(163, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:01:17', '2019-11-10 03:01:17');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(164, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:02:42', '2019-11-10 03:02:42');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(165, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:07:58', '2019-11-10 03:07:58');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(166, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:09:05', '2019-11-10 03:09:05');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(167, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:11:32', '2019-11-10 03:11:32');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(168, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:14:21', '2019-11-10 03:14:21');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(169, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:17:10', '2019-11-10 03:17:10');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(170, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:30:59', '2019-11-10 03:30:59');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(171, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:34:05', '2019-11-10 03:34:05');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(172, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:34:58', '2019-11-10 03:34:58');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(173, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:37:52', '2019-11-10 03:37:52');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(174, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:39:47', '2019-11-10 03:39:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(175, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:41:17', '2019-11-10 03:41:17');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(176, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:42:05', '2019-11-10 03:42:05');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(177, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:42:59', '2019-11-10 03:42:59');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(178, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:43:33', '2019-11-10 03:43:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(179, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:52:16', '2019-11-10 03:52:16');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(180, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:53:47', '2019-11-10 03:53:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(181, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:58:28', '2019-11-10 03:58:28');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(182, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:59:01', '2019-11-10 03:59:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(183, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 03:59:53', '2019-11-10 03:59:53');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(184, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:01:31', '2019-11-10 04:01:31');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(185, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:02:10', '2019-11-10 04:02:10');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(186, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:03:58', '2019-11-10 04:03:58');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(187, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:06:06', '2019-11-10 04:06:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(188, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:06:56', '2019-11-10 04:06:56');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(189, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:07:29', '2019-11-10 04:07:29');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(190, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:10:38', '2019-11-10 04:10:38');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(191, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:12:42', '2019-11-10 04:12:42');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(192, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:13:21', '2019-11-10 04:13:21');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(193, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:13:45', '2019-11-10 04:13:45');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(194, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 04:14:15', '2019-11-10 04:14:15');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(195, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:01:33', '2019-11-10 06:01:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(196, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:02:23', '2019-11-10 06:02:23');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(197, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:11:11', '2019-11-10 06:11:11');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(198, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:12:11', '2019-11-10 06:12:11');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(199, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:12:33', '2019-11-10 06:12:33');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(200, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:12:56', '2019-11-10 06:12:56');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(201, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:13:17', '2019-11-10 06:13:17');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(202, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:13:38', '2019-11-10 06:13:38');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(203, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:15:49', '2019-11-10 06:15:49');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(204, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:17:24', '2019-11-10 06:17:24');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(205, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:17:43', '2019-11-10 06:17:43');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(206, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:17:55', '2019-11-10 06:17:55');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(207, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:18:29', '2019-11-10 06:18:29');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(208, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:18:49', '2019-11-10 06:18:49');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(209, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:19:07', '2019-11-10 06:19:07');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(210, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:21:09', '2019-11-10 06:21:09');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(211, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:22:53', '2019-11-10 06:22:53');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(212, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:23:23', '2019-11-10 06:23:23');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(213, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:23:41', '2019-11-10 06:23:41');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(214, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:23:56', '2019-11-10 06:23:56');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(215, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:24:19', '2019-11-10 06:24:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(216, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:27:47', '2019-11-10 06:27:47');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(217, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:28:08', '2019-11-10 06:28:08');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(218, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:28:37', '2019-11-10 06:28:37');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(219, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:28:50', '2019-11-10 06:28:50');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(220, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:29:01', '2019-11-10 06:29:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(221, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:34:22', '2019-11-10 06:34:22');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(222, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:34:40', '2019-11-10 06:34:40');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(223, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:36:19', '2019-11-10 06:36:19');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(224, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:37:59', '2019-11-10 06:37:59');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(225, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:38:58', '2019-11-10 06:38:58');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(226, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:41:27', '2019-11-10 06:41:27');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(227, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:41:45', '2019-11-10 06:41:45');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(228, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:42:26', '2019-11-10 06:42:26');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(229, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:42:40', '2019-11-10 06:42:40');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(230, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:43:03', '2019-11-10 06:43:03');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(231, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:43:15', '2019-11-10 06:43:15');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(232, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:44:01', '2019-11-10 06:44:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(233, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:47:54', '2019-11-10 06:47:54');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(234, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:50:06', '2019-11-10 06:50:06');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(235, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:51:01', '2019-11-10 06:51:01');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(236, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:52:26', '2019-11-10 06:52:26');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(237, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:53:16', '2019-11-10 06:53:16');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(238, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:54:03', '2019-11-10 06:54:03');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(239, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:54:43', '2019-11-10 06:54:43');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(240, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 06:55:08', '2019-11-10 06:55:08');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(241, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 07:24:58', '2019-11-10 07:24:58');
INSERT INTO `auth_logs` (`id`, `origin_ip`, `origin_merchant_id`, `app_id`, `auth_header`, `message_type`, `response_code`, `created_at`, `updated_at`) VALUES(242, '::1', 1, 3, 'OWM2YTY2NzctMmQ0YS00NzNhLWJiNTMtMWRmMDlhZDAzMmIxOmUxZGVkNDY5LTY1NGQtNDJlOS1mNTQ2LWRlNTIxYzYwYmMzMA==', 'AUTH_OK', 9800, '2019-11-10 07:25:10', '2019-11-10 07:25:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `configs`
--

DROP TABLE IF EXISTS `configs`;
CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `partner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `partner_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `station_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `terminal_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) DEFAULT '0',
  `merchant_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `configs_merchant_id_index` (`merchant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `configs`
--

INSERT INTO `configs` (`id`, `partner_id`, `partner_secret`, `station_id`, `terminal_id`, `api_url`, `currency`, `timezone`, `is_active`, `merchant_id`, `created_at`, `updated_at`) VALUES(1, 'TabsquareHIT_Dev', '4ad9e842fa1950007dfdaf5aaac55f86ae9ff77096d3884945591f09e589071b', '4080602107', 'k1', 'https://uat.windcave.com/pxmi3/pos.aspx', 'NZD', '', 1, 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `configs` (`id`, `partner_id`, `partner_secret`, `station_id`, `terminal_id`, `api_url`, `currency`, `timezone`, `is_active`, `merchant_id`, `created_at`, `updated_at`) VALUES(2, 'TabsquareHIT_Dev', '4ad9e842fa1950007dfdaf5aaac55f86ae9ff77096d3884945591f09e589071b', '4080602108', 'K2', 'https://uat.windcave.com/pxmi3/pos.aspx', 'NZD', '', 1, 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_id_gopay` bigint(20) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `total_amount` decimal(10,2) NOT NULL,
  `table_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `msg_id` text COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gopay_txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qr_code` text COLLATE utf8_unicode_ci,
  `url_gopay_status` text COLLATE utf8_unicode_ci NOT NULL,
  `url_gopay_cancel` text COLLATE utf8_unicode_ci NOT NULL,
  `url_gopay_update` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1 - Success Generate, 2 - Success Reversal  3 - Sucess Refund, 4 - Success Query',
  `request` text COLLATE utf8_unicode_ci NOT NULL,
  `response` text COLLATE utf8_unicode_ci NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `client_info` text COLLATE utf8_unicode_ci NOT NULL,
  `op_type` tinyint(4) NOT NULL,
  `app_id` tinyint(4) NOT NULL,
  `txn_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_order_id_index` (`order_id`),
  KEY `logs_order_id_gopay_index` (`order_id_gopay`),
  KEY `logs_merchant_id_index` (`merchant_id`),
  KEY `logs_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(12, '2019_09_03_072530_create_apps_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(13, '2019_09_03_072621_create_app_permissions_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(14, '2019_09_03_073458_create_configs_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(15, '2019_09_03_073544_create_logs_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(16, '2019_09_03_073736_create_transactions_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES(17, '2019_11_08_171826_create_auth_logs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_id_gopay` bigint(20) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `total_amount` decimal(10,2) NOT NULL,
  `table_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `msg_id` text COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gopay_txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qr_code` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL COMMENT '0 - Unpaid, 1 - Paid, 2 - Reversed',
  `merchant_id` int(11) NOT NULL,
  `client_info` text COLLATE utf8_unicode_ci NOT NULL,
  `app_id` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_order_id_index` (`order_id`),
  KEY `transactions_order_id_gopay_index` (`order_id_gopay`),
  KEY `transactions_merchant_id_index` (`merchant_id`),
  KEY `transactions_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
