-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 01 Apr 2020 pada 22.26
-- Versi server: 5.7.26
-- Versi PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `windcave`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `configs`
--

DROP TABLE IF EXISTS `configs`;
CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `partner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `partner_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `station_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `device_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pos_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pos_version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `terminal_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) DEFAULT '0',
  `merchant_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `configs_merchant_id_index` (`merchant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `configs`
--

INSERT INTO `configs` (`id`, `partner_id`, `partner_secret`, `station_id`, `device_id`, `vendor_id`, `pos_name`, `pos_version`, `terminal_id`, `api_url`, `currency`, `timezone`, `is_active`, `merchant_id`, `created_at`, `updated_at`) VALUES(1, 'TabsquareHIT_Dev', '4ad9e842fa1950007dfdaf5aaac55f86ae9ff77096d3884945591f09e589071b', '4080602107', 'DEVICE-TS1', '12345', 'POS-TS1', '1.0', 'K1', 'https://uat.windcave.com/pxmi3/pos.aspx', 'NZD', 'AUS', 1, 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
INSERT INTO `configs` (`id`, `partner_id`, `partner_secret`, `station_id`, `device_id`, `vendor_id`, `pos_name`, `pos_version`, `terminal_id`, `api_url`, `currency`, `timezone`, `is_active`, `merchant_id`, `created_at`, `updated_at`) VALUES(2, 'TabsquareHIT_Dev', '4ad9e842fa1950007dfdaf5aaac55f86ae9ff77096d3884945591f09e589071b', '3801557984', 'DEVICE-TS2', '12345', 'POS-TS2', '1.0', 'K2', 'https://uat.windcave.com/pxmi3/pos.aspx', 'NZD', 'AUS', 1, 1, '2019-09-04 03:49:11', '2019-09-04 03:49:11');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
