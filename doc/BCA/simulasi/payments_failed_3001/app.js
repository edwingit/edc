var express = require("express");
var app = express();
app.listen(3000, () => {
 console.log("Server running on port 3000");
});

//sample:http://localhost:3000/payment/failed?order=1234&amount=2000
app.get("/payment/success", (req, res, next) => {
    console.log("OrderNo : "+req.query.order)
    console.log("Amount : "+req.query.amount)
    res.json({"data":{"STAN":100709,"MerchantId":"000885000015999","TerminalID":"DDDEV034","BankDateTime":"2019-04-01T11:08:42","TxnRef":"101131","CardPAN":"601900******4038   ","Receipt":null,"CardType":"FLAZZ","AuthCode":49051,"status":"Approve","ID":req.query.order,"result":"APPROVED","surcharge":0.0},"message":"The transaction settled successfully!","nextCommand":"","result":true});
});

//sample:http://localhost:3000/payment/failed?order=1234&amount=2000
app.get("/payment/failed", (req, res, next) => {
 console.log("OrderNo : "+req.query.order)
 console.log("Amount : "+req.query.amount)
 res.json({"data":{"STAN":0,"MerchantId":"000885000015999","TerminalID":"DDDEV034 ","BankDateTime":"2019-04-01T11:04:12","TxnRef":"            ","CardPAN":"                   ","Receipt":null,"CardType":"OTHER","AuthCode":0,"status":"EDC Problem","ID":req.query.order,"result":"FAILED","surcharge":0.0},"message":"The transaction settled successfully!","nextCommand":"","result":false});
});

//sample:http://localhost:3000/payment/failed?order=1234&amount=2000
app.get("/payment/to", (req, res, next) => {
    console.log("OrderNo : "+req.query.order)
    console.log("Amount : "+req.query.amount)
    res.json({"data":{"STAN":0,"MerchantId":"000885000015999","TerminalID":"DDDEV034","BankDateTime":"2019-04-01T11:09:46","TxnRef":"            ","CardPAN":"                   ","Receipt":null,"CardType":"OTHER","AuthCode":0,"status":"Connection Timeout","ID":req.query.order,"result":"FAILED","surcharge":0.0},"message":"The transaction settled successfully!","nextCommand":"","result":false});
});